// No.1 - Mengubah fungsi menjadi fungsi arrow
var golden = () => {
    console.log("this is golden!!")
}
  golden()
console.log("")

// No.2 - Sederhanakan menjadi Object literal di ES6
newFunction = (firstName, lastName) => {
    return {
    firstName
    lastName
    return 
    {
    fullName(){
    console.log(firstName + " " + lastName)
    }
    }
    }
    //Driver Code
    newFunction("William", "Imoh").fullName()
    console.log("")

// No.3 - Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
    }
const { firstName, lastName, destination, occupation, speel }  = newObject;
console.log(firstName, lastName, destination, occupation, speel)
console.log("")

// No.4 - Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combinedArray = [... west, ...east]
//Driver Code
console.log(combinedArray)
console.log("")

// No.5 - Template Literals
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit  ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before);
console.log("")